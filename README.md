MOTD (message of the day)
=========================

This is a stub until a more sophisticated motd mechanism can be implemented in
the future, with better platform integration.

Providers can opt-in to the motd feature (only riseup is using it at the moment).

If motd is enabled for a given provider, the client will attempt to fetch
the motd.json file from a well-known URL, and will display the first valid
message on the splash screen.

The structure of the `motd.json` file is like follows:

```
{
    "motd": [{
        "begin":    "01 Nov 21 00:00 -0700",
        "end":      "31 Jan 22 00:00 -0700",
        "type":     "daily",
        "platform": "all",
        "urgency":  "normal",
        "text": [
          { "lang": "en",
            "str": "This is a <a href='https://leap.se'>test!</a>"},
          { "lang": "es",
            "str": "Esto es una <a href='https://leap.se'>pruebita!</a>"}
        ]}
    ]
}
```

Valid values are: 

* Begin, End are date strings, in the format: "01 Jan 21 00:00:00 -0700".
* Type: "once" for a one-shot message, "daily" for a message that is displayed daily during the specified duration.
* Platform: one of "windows", "osx", "snap", "linux", "android" or "all".
* Urgency: either "normal" or "critical".

The text message can contain links.

You can use the [motd-cli](https://0xacab.org/leap/bitmask-vpn/-/blob/main/branding/motd-cli/README.md) tool to parse and validate the json. Check out that repository to get more infos about the cli.

# Development
## Getting the motd
Using this lib you can obtain the messages of the day by calling 
```func FetchLatestFrom(url string, provider string, platform string) Messages```

```func FetchLatest() []Message```

or using the `Motd` struct
```func (m *Motd) FetchLatestAsJson() string``` 

All messages will be returned presorted by urgency and begin time. Messages with a different platform type than the specified or `all` are filtered out.

## Handling motd's
To get the most relevant message of the day you can call

```func (m *Messages) GetFirstMessage(lastSeen string, onceSeenHashes IStringCollection) IMessage {```

Messages of type `once` need some extra love. In order to make sure the don't get picked up twice, hashes of those messages are passed as a parameter (`onceSeenHashes IStringCollection`). 
Therefore `IMessage` defines a `Hash()` function, which the `Message` struct implements.
It's the integrators responsibility to persist these hashes and to add the hash of the Message returned by `GetFirstMessage` to this collection!
Similarly, the integrator has to handle the timestamp of the last shown message separately. 

## FFI
The FFI for this lib is build with gomobile. In order to use the model structs over the FFI they need to implement interfaces. As a naming convention the interfaces begin with a capital letter "I", followed by the struct name which implement them, e.g. `type IMessage interface` and `type Message struct`.

Interfaces are passed to the foreign languages instead of structs. This way we work around the lack of support for different types [as slices](https://github.com/golang/go/issues/13445) in gomobile. As a drawback we have to write some more boiler plate code and define getter functions in the interfaces to expose properties of a struct. Check this example:

```
type IMessage interface {
  ...
	MPlatform() string
	MUrgency() string
	MType() string
	...
}

type Message struct {
  ...
	Type     string          `json:"type"`
	Platform string          `json:"platform"`
	Urgency  string          `json:"urgency"`
  ...
}

func (m *Message) MPlatform() string {
	return m.Platform
}

func (m *Message) MUrgency() string {
	return m.Urgency
}

func (m *Message) MType() string {
	return m.Type
}
```

By convention, getter functions for properties do not begin with `Get` but with the capital letter `M` (see above, e.g. `func (m *Message) MPlatform() string`). Gomobile already generates getter methods for properties of a struct with the prefix `Get`, which would result in naming conflicts if our interfaces declared the same method names. As a reminder: We're passing interfaces to the foreign language, and don't use the generated foreign language code for structs. 


