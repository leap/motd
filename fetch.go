package motd

import (
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"sort"

	"0xacab.org/leap/bitmask-vpn/pkg/config"
	_ "golang.org/x/mobile/app" // logs to Android logcat
)

const riseupMOTD = "https://static.riseup.net/vpn/motd.json"
const TimeString = "02 Jan 06 15:04 -0700" // RFC822 with numeric zone
const PLATFORM_ALL = "all"
const PLATFORM_ANDROID = "android"
const PLATFORM_LINUX = "linux"
const PLATFORM_MACOS = "osx"
const PLATFORM_WINDOWS = "windows"
const PLATFORM_LINUX_SNAP = "snap"

func FetchLatest() []Message {
	empty := []Message{}
	if os.Getenv("SKIP_MOTD") == "1" {
		return empty
	}
	url := ""
	if os.Getenv("DEBUG") == "1" {
		url = os.Getenv("MOTD_URL")
		if url == "" {
			url = riseupMOTD
		}
	} else {
		switch config.Provider {
		case "riseup.net":
			url = riseupMOTD
		default:
			return empty
		}
	}

	if url == "" {
		return empty
	}

	return FetchLatestFrom(url, config.Provider, PLATFORM_ALL).Messages
}

// FetchLatestFrom - fetches all messages of a day, filters them for a given platfrom,
// sanitizes and sorts them by Urgency and Begin date.
// FFI
func FetchLatestFrom(url string, provider string, platform string) Messages {
	empty := []Message{}
	emptyMessages := &Messages{
		Messages: empty,
	}

	if url == "" {
		return *emptyMessages
	}

	log.Println("Fetching MOTD for", provider)
	b, err := fetchURL(url)
	if err != nil {
		log.Println("WARN Error fetching json from", url)
		return *emptyMessages
	}
	allMsg, err := getFromJSON(b)
	if err != nil {
		log.Println("WARN Error parsing json from", url)
		return *emptyMessages
	}
	valid := empty[:]
	if allMsg.Length() != 0 {
		log.Printf("There are %d pending messages\n", allMsg.Length())
	}
	for _, msg := range allMsg.Messages {
		if msg.IsValid() && msg.IsSupportedPlatform(platform) {
			valid = append(valid, msg)
		}
	}

	allMsg.Messages = sortByUrgencyAndBegin(valid)
	return allMsg
}

func sortByUrgencyAndBegin(messages []Message) []Message {
	sort.Slice(messages, func(i, j int) bool {
		beginTimeI := messages[i].GetBeginTime()
		beginTimeJ := messages[j].GetBeginTime()
		if beginTimeI == nil {
			return false
		}
		if beginTimeJ == nil {
			return true
		}
		return beginTimeI.After(*beginTimeJ)
	})

	sort.Slice(messages, func(i, j int) bool {
		return messages[i].UrgencyAsInt() > messages[j].UrgencyAsInt()
	})
	return messages
}

func fetchURL(url string) ([]byte, error) {
	resp, err := http.Get(url)
	if err != nil {
		return []byte(""), err
	}
	defer resp.Body.Close()
	return ioutil.ReadAll(resp.Body)
}
