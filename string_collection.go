package motd

// IStringCollection - interface definition for String arrays to support FFI communication
type IStringCollection interface {
	Add(item string)
	Get(index int) string
	Length() int
}

// StringCollection - a gomobile FFI conform wrapper around a string array
// conforms to StringCollection
type StringCollection struct {
	items []string
}

func NewStringCollection() IStringCollection {
	return &StringCollection{
		items: []string{},
	}
}

func (s *StringCollection) Add(item string) {
	s.items = append(s.items, item)
}

func (s *StringCollection) Get(index int) string {
	return s.items[index]
}

func (s *StringCollection) Length() int {
	return len(s.items)
}
