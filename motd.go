package motd

import (
	"encoding/json"
	"io/ioutil"
	"os"
)

func ParseFile(f string) (Messages, error) {
	jsonFile, err := os.Open(f)
	if err != nil {
		return Messages{}, err
	}
	defer jsonFile.Close()
	byteVal, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		return Messages{}, err
	}
	return getFromJSON(byteVal)
}

func getFromJSON(b []byte) (Messages, error) {
	var m Messages
	err := json.Unmarshal(b, &m)
	return m, err
}

type IMotd interface {
	FetchLatestAsJson() string
}

// Motd - a Message of the day client struct
type Motd struct {
	SourceUrl string
	Provider  string
	Platform  string
}

func NewMotd(provider, sourceUrl, platform string) IMotd {
	return &Motd{
		Provider:  provider,
		SourceUrl: sourceUrl,
		Platform:  platform,
	}
}

func (m *Motd) FetchLatestAsJson() string {
	messages := FetchLatestFrom(m.Provider, m.SourceUrl, m.Platform)
	jsonBytes, err := json.Marshal(messages)
	if err == nil {
		return string(jsonBytes)
	}

	return ""
}
