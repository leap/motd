package motd

import (
	"crypto/md5"
	"encoding/json"
	"fmt"
	"log"
	"time"

	_ "golang.org/x/mobile/app" // logs to Android logcat
)

const MESSAGE_TYPE_ONCE = "once"
const MESSAGE_TYPE_DAILY = "daily"
const MESSAGE_URGENCY_NORMAL = "normal"
const MESSAGE_URGENCY_CRITICAL = "critical"

type IMessage interface {
	GetLocalizedText(langKey string) string
	Hash() string
	IsValid() bool
	MPlatform() string
	MUrgency() string
	MType() string
	ToJson() string
}

type Message struct {
	Begin    string          `json:"begin"`
	End      string          `json:"end"`
	Type     string          `json:"type"`
	Platform string          `json:"platform"`
	Urgency  string          `json:"urgency"`
	Text     []LocalizedText `json:"text"`
}

func NewMessage(jsonString string) IMessage {
	var m Message
	err := json.Unmarshal([]byte(jsonString), &m)

	if err != nil {
		return nil
	}

	return &m
}

func (m *Message) GetLocalizedText(langKey string) string {
	for i := 0; i < len(m.Text); i++ {
		if m.Text[i].Lang == langKey {
			return m.Text[i].Str
		}
	}
	return ""
}

func (m *Message) ToJson() string {
	res, err := json.Marshal(m)
	if err != nil {
		log.Println(err)
	}
	return string(res)
}

func (m *Message) Hash() string {
	value := fmt.Sprintf("%+v", m)
	hmd5 := md5.Sum([]byte(value))
	return fmt.Sprintf("%x", hmd5)
}

func (m *Message) IsValid() bool {
	valid := (m.IsValidBegin() && m.IsValidEnd() &&
		m.IsValidType() && m.IsValidPlatform() && m.IsValidUrgency() &&
		m.HasLocalizedText())
	return valid
}

func (m *Message) MPlatform() string {
	return m.Platform
}

func (m *Message) MUrgency() string {
	return m.Urgency
}

func (m *Message) MType() string {
	return m.Type
}

func (m *Message) HasLocalizedText() bool {
	return len(m.Text) > 0
}

func (m *Message) GetBeginTime() *time.Time {
	time, err := time.Parse(TimeString, m.Begin)
	if err != nil {
		return nil
	}
	return &time
}

func (m *Message) GetEndTime() *time.Time {
	endTime, err := time.Parse(TimeString, m.End)
	if err != nil {
		return nil
	}
	return &endTime
}

func (m *Message) IsValidBegin() bool {
	_, err := time.Parse(TimeString, m.Begin)
	if err != nil {
		log.Println(err)
		return false
	}
	return true
}

func (m *Message) IsValidEnd() bool {
	endTime, err := time.Parse(TimeString, m.End)
	if err != nil {
		log.Println(err)
		return false
	}
	beginTime, err := time.Parse(TimeString, m.Begin)
	if err != nil {
		log.Println(err)
		return false
	}
	if !beginTime.Before(endTime) {
		log.Println("begin ts should be before end")
		return false
	}
	return true
}

func (m *Message) IsValidType() bool {
	switch m.Type {
	case MESSAGE_TYPE_ONCE, MESSAGE_TYPE_DAILY:
		return true
	default:
		return false
	}
}

func (m *Message) IsValidPlatform() bool {
	switch m.Platform {
	case PLATFORM_WINDOWS, PLATFORM_LINUX, PLATFORM_MACOS, PLATFORM_ANDROID, PLATFORM_ALL:
		return true
	default:
		return false
	}
}

func (m *Message) IsSupportedPlatform(platform string) bool {
	return m.Platform == platform || m.Platform == PLATFORM_ALL
}

func (m *Message) IsValidUrgency() bool {
	switch m.Urgency {
	case MESSAGE_URGENCY_NORMAL, MESSAGE_URGENCY_CRITICAL:
		return true
	default:
		return false
	}
}

func (m *Message) UrgencyAsInt() int {
	switch m.Urgency {
	case "normal":
		return 0
	case "critical":
		return 1
	default:
		return -1
	}
}

type LocalizedText struct {
	Lang string `json:"lang"`
	Str  string `json:"str"`
}
