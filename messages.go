package motd

import (
	"encoding/json"
	"log"
	"time"

	_ "golang.org/x/mobile/app" // logs to Android logcat
)

type IMessages interface {
	ToJson() string
	Length() int
	Get(i int) IMessage
	GetFirstMessage(lastSeen string, onceSeenHashes IStringCollection) IMessage
}

func NewMessages(json string) IMessages {
	messages, err := getFromJSON([]byte(json))
	if err != nil {
		return nil
	}
	return &messages
}

// Messages is a json and gomobile FFI conform Wrapper class around an Messages array
type Messages struct {
	Messages []Message `json:"motd"`
}

func (m *Messages) ToJson() string {
	res, err := json.Marshal(m)
	if err != nil {
		log.Println(err)
	}
	return string(res)
}

func (m *Messages) Length() int {
	return len(m.Messages)
}

func (m *Messages) Get(i int) IMessage {
	return &m.Messages[i]
}

// GetFirstMessage - returns the first applicable message, it filters out outdated or already seen messages
// lastSeen - the date string indicating the last time a message of the day was shown
// onceSeenHashes - hash values of seen messages of type once
func (m *Messages) GetFirstMessage(lastSeen string, onceSeenHashes IStringCollection) IMessage {
	lastSeenTime, err := time.Parse(TimeString, lastSeen)
	if err != nil {
		return nil
	}

	// m.Messages is presorted by urgency and BeginTime
	for _, message := range m.Messages {
		// Skip already seen messages of type once
		if message.Type == "once" {
			hash := message.Hash()
			messageWasSeen := false
			for i := 0; i < onceSeenHashes.Length(); i++ {
				if onceSeenHashes.Get(i) == hash {
					messageWasSeen = true
					break
				}
			}

			if messageWasSeen {
				continue
			}
		}

		// Skip outdated critical messages
		messageBegin := message.GetBeginTime()
		messageEnd := message.GetEndTime()
		if message.Urgency == "critical" && messageEnd.Before(lastSeenTime) {
			continue
		}

		if messageBegin.After(lastSeenTime) {
			return &message
		}
	}

	return nil
}
