package motd

import (
	"testing"
)

const ExampleFile = "motd-example.json"

func TestGoodMotd(t *testing.T) {
	m, err := ParseFile(ExampleFile)
	if err != nil {
		t.Errorf("error parsing default file")
	}
	if m.Length() == 0 {
		t.Errorf("zero messages in file")
	}
	for _, msg := range m.Messages {
		if !msg.IsValid() {
			t.Errorf("invalid motd json at %s", ExampleFile)
		}
	}
}

const emptyDate = `
{
    "motd": [{
        "begin":    "",
        "end":      "",
        "type":     "daily",
        "platform": "all",
        "urgency":  "normal",
        "text": [
          { "lang": "en",
            "str": "test"
	  }]
    }]
}`

func TestEmptyDateFails(t *testing.T) {
	m, err := getFromJSON([]byte(emptyDate))
	if err != nil {
		t.Errorf("error parsing json")
	}
	if allValid(t, m) {
		t.Errorf("empty string should not be valid")
	}
}

const badEnd = `
{
    "motd": [{
	"begin":    "02 Jan 21 00:00 +0100",
	"end":      "01 Jan 21 00:00 +0100",
        "type":     "daily",
        "platform": "all",
        "urgency":  "normal",
        "text": [
          { "lang": "en",
            "str": "test"
	  }]
    }]
}`

func TestBadEnd(t *testing.T) {
	m, err := getFromJSON([]byte(badEnd))
	if err != nil {
		t.Errorf("error parsing json")
	}
	if allValid(t, m) {
		t.Errorf("begin > end must fail")
	}
}

func allValid(t *testing.T, m Messages) bool {
	if m.Length() == 0 {
		t.Errorf("expected at least one message")

	}
	for _, msg := range m.Messages {
		if !msg.IsValid() {
			return false
		}
	}
	return true
}

const multiple_unsorted = `
{
    "motd": [{
		"begin":    "10 Jan 21 00:00 +0100",
		"end":      "12 Jan 21 00:00 +0100",
			"type":     "daily",
			"platform": "all",
			"urgency":  "normal",
			"text": [
			{ "lang": "en",
				"str": "test3"
			}]
    }, {
		"begin":    "01 Jan 21 00:00 +0100",
		"end":      "02 Jan 21 00:00 +0100",
			"type":     "daily",
			"platform": "all",
			"urgency":  "normal",
			"text": [
				{ "lang": "en",
				"str": "test1"
			}]
	}, {
		"begin":    "03 Jan 21 00:00 +0100",
		"end":      "04 Jan 21 00:00 +0100",
			"type":     "daily",
			"platform": "all",
			"urgency":  "normal",
			"text": [
				{ "lang": "en",
				"str": "test2"
			}]
	}]
}`

func TestSortForDate(t *testing.T) {
	m, err := getFromJSON([]byte(multiple_unsorted))
	if err != nil {
		t.Errorf("error parsing json")
	}
	messages := sortByUrgencyAndBegin(m.Messages)
	if !(messages[0].Text[0].Str == "test3" && messages[1].Text[0].Str == "test2" && messages[2].Text[0].Str == "test1") {
		t.Logf(messages[0].Text[0].Str)
		t.Logf(messages[1].Text[0].Str)
		t.Logf(messages[2].Text[0].Str)
		t.Errorf("invalid sorting")
	}
}

const multiple_unsorted_different_criticality = `
{
    "motd": [{
		"begin":    "10 Jan 21 00:00 +0100",
		"end":      "12 Jan 21 00:00 +0100",
			"type":     "daily",
			"platform": "all",
			"urgency":  "normal",
			"text": [
			{ "lang": "en",
				"str": "test3"
			}]
    }, {
		"begin":    "01 Jan 21 00:00 +0100",
		"end":      "12 Jan 21 00:00 +0100",
			"type":     "daily",
			"platform": "all",
			"urgency":  "normal",
			"text": [
				{ "lang": "en",
				"str": "test1"
			}]
	}, {
		"begin":    "03 Jan 21 00:00 +0100",
		"end":      "14 Jan 21 00:00 +0100",
			"type":     "daily",
			"platform": "all",
			"urgency":  "critical",
			"text": [
				{ "lang": "en",
				"str": "test2"
			}]
	}]
}`

func TestSortForDateAndCriticality(t *testing.T) {
	m, err := getFromJSON([]byte(multiple_unsorted_different_criticality))
	if err != nil {
		t.Errorf("error parsing json")
	}
	messages := sortByUrgencyAndBegin(m.Messages)
	if !(messages[0].Text[0].Str == "test2" && messages[1].Text[0].Str == "test3" && messages[2].Text[0].Str == "test1") {
		t.Logf(messages[0].Text[0].Str)
		t.Logf(messages[1].Text[0].Str)
		t.Logf(messages[2].Text[0].Str)
		t.Errorf("invalid sorting")
	}
}

const multiple_unsorted_once = `
{
    "motd": [{
		"begin":    "01 Jan 21 00:00 +0100",
		"end":      "12 Jan 21 00:00 +0100",
			"type":     "daily",
			"platform": "all",
			"urgency":  "normal",
			"text": [
				{ "lang": "en",
				"str": "test1"
			}]
	}, {
		"begin":    "03 Jan 21 00:00 +0100",
		"end":      "14 Jan 21 00:00 +0100",
			"type":     "daily",
			"platform": "all",
			"urgency":  "normal",
			"text": [
				{ "lang": "en",
				"str": "test2"
			}]
	}, {
		"begin":    "10 Jan 21 00:00 +0100",
		"end":      "12 Jan 21 00:00 +0100",
			"type":     "once",
			"platform": "all",
			"urgency":  "normal",
			"text": [
			{ "lang": "en",
				"str": "test3"
			}]
    }]
}`

func TestGetFirstMessage(t *testing.T) {
	m, err := getFromJSON([]byte(multiple_unsorted_once))
	if err != nil {
		t.Errorf("error parsing json")
	}

	//when fetching from the API, we sort the entries
	messages := sortByUrgencyAndBegin(m.Messages)
	m.Messages = messages

	message := m.GetFirstMessage("02 Jan 21 00:00 +0100", NewStringCollection())
	if message.GetLocalizedText("en") != "test3" {
		t.Logf(message.GetLocalizedText("en"))
		t.Errorf("invalid first message")
	}

}

func TestGetFirstMessage_ignoreSeenMessages(t *testing.T) {
	m, err := getFromJSON([]byte(multiple_unsorted_once))
	if err != nil {
		t.Errorf("error parsing json")
	}

	//when fetching from the API, we sort the entries
	messages := sortByUrgencyAndBegin(m.Messages)
	m.Messages = messages

	stringCollection := NewStringCollection()
	stringCollection.Add("7ee5097e7c755660741bc50432d680c3")
	message := m.GetFirstMessage("02 Jan 21 00:00 +0100", stringCollection)
	if message.GetLocalizedText("en") != "test2" {
		t.Logf(message.GetLocalizedText("en"))
		t.Errorf("invalid first message - test3 should be ignored")
	}

}
